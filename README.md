# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://evilprime@bitbucket.org/evilprime/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/evilprime/stroboskop/commits/1e5e3d353ee62e72000f21e9424dacc9a3046256

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/evilprime/stroboskop/commits/873434b32268e2c74dc0847aabe5a2b80dc4ce80

Naloga 6.3.2:
https://bitbucket.org/evilprime/stroboskop/commits/b9ce2aee0733f3277d81c9d883ded2f5ed95ad1d

Naloga 6.3.3:

Naloga 6.3.4:

Naloga 6.3.5:

```
git checkout master
git merge izgled
git pull
git push origin master

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:


Naloga 6.4.2:
((UVELJAVITEV))

Naloga 6.4.3:
((UVELJAVITEV))

Naloga 6.4.4:
((UVELJAVITEV))